package org.midoribrowser.android

enum class BrowserMode {
    Normal,
    Incognito
}
